import Modal from "../Modal.js";
import { dataService } from "../services.js";
import { VisitView } from "../visitController.js";

export default class VisitModal extends Modal {
   constructor(modalSelector, triggerBtnSelector) {
      super(modalSelector, triggerBtnSelector);
      this.modal = document.querySelector(modalSelector);
      this.form = this.modal.querySelector('form');
      this.doctorSelect = this.modal.querySelector('#doctor-select');
      this.onSubmit = this.onSubmit.bind(this);
      this.form.addEventListener('submit', event => {
         event.preventDefault();
         this.onSubmit();
      });
      this.addDoctorSelectListener();
   }

   // слухач на випадаючий список вибору лікаря
   addDoctorSelectListener() {
      this.doctorSelect.addEventListener('change', this.showFieldsForDoctor.bind(this));
   }

   // показ полів в залежності від обраного лікаря
   showFieldsForDoctor() {
      const selectedDoctor = this.doctorSelect.value;
      const doctorFields = this.modal.querySelector("#doctor-fields");
      const otherFields = this.modal.querySelectorAll(
         "#cardiologist-fields, #dentist-fields, #therapist-fields"
      );

      otherFields.forEach(field => field.style.display = "none");
      doctorFields.style.display = "block";

      if (selectedDoctor === "cardiologist") {
         this.modal.querySelector("#cardiologist-fields").style.display = "block";
      } else if (selectedDoctor === "dentist") {
         this.modal.querySelector("#dentist-fields").style.display = "block";
      } else if (selectedDoctor === "therapist") {
         this.modal.querySelector("#therapist-fields").style.display = "block";
      }
   }

   // отримання даних з інпутів
   getFormData() {
      const data = {};
      const doctorFields = document.getElementById('doctor-fields');
      const cardiologistFields = document.getElementById('cardiologist-fields');
      const dentistFields = document.getElementById('dentist-fields');
      const therapistFields = document.getElementById('therapist-fields');

      data['doctor'] = document.getElementById('doctor-select').value;
      data['purpose'] = doctorFields.querySelector('[name="purpose"]').value;
      data['description'] = doctorFields.querySelector('[name="description"]').value;
      data['urgency'] = doctorFields.querySelector('[name="urgency"]').value;
      data['name'] = doctorFields.querySelector('[name="name"]').value;
console.log(data);
      if (cardiologistFields.style.display === 'block') {
         data['pressure'] = cardiologistFields.querySelector('[name="pressure"]').value;
         data['bmi'] = cardiologistFields.querySelector('[name="bmi"]').value;
         data['heartDisease'] = cardiologistFields.querySelector('[name="heartDisease"]').value;
         data['age'] = cardiologistFields.querySelector('[name="cardio-age"]').value;
      }

      if (dentistFields.style.display !== 'none') {
         data['lastVisit'] = dentistFields.querySelector('[name="lastVisit"]').value;
      }

      if (therapistFields.style.display !== 'none') {
         data['age'] = therapistFields.querySelector('[name="therapist-age"]').value;
      }

      return data;
   }


   // Заготовка методу для перевірки валідності полів
   checkFields(formData) {
      for (const key in formData) {
         if (formData[key] === undefined || formData[key].trim() === '') {
            console.log(`Будь ласка, заповніть поле ${key}!`);
            return false;
         }
      }
      return true;
   }


   onSubmit() {
      // Отримуємо дані з форми
      const formData = this.getFormData();

      // Перевірка, на пусті поля
      if (!this.checkFields(formData)) {
         return;
      }
      new VisitView().setVisit(formData);
      // new VisitView().editVisit(formData);
      // dataService.createVisit(formData).then((res)=>{
      //    if(res.ok) {
      //       return res.json();
      //    } else {
      //       throw new Error(
      //          'Error res'
      //       );
      //    }
      // }).then(obj => {
      //    console.log(obj);
      // })
      // .catch((error) => console.log(error));

      // Ось тут потрібно створити клас Visit
      console.log(formData);
      this.form.reset();
      this.close();
   }
}