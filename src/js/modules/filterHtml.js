import { status, urgency } from "../constants.js";
import { filterVisits as filterController } from "../modules/filterCards.js";

export class FilterSection extends FormElement {
  //loginModal
  constructor() {
    super();
    this.html = `
		<form id="filter-form">
      <span class="filter-parts">Search:</span>
      <label><input id="searchByKeyword" type="text" name="contentSearch" value=""></label>
      <span class="filter-parts">Status:</span>
      <label><input type="checkbox" name = "visitStatus" value="${status.open}">${status.open}</label>
      <label><input type="checkbox"  name = "visitStatus" value="${status.done}">${status.done}</label>
      <span class="filter-parts">Priority:</span>
      <label><input type="checkbox"  name = "visitUrgency" value="${urgency.urgent}">${urgency.urgent}</label>
      <label><input type="checkbox"  name = "visitUrgency" value="${urgency.important}">${urgency.important}</label>
      <label><input type="checkbox"  name = "visitUrgency" value="${urgency.common}">${urgency.common}</label>
      <input class="reset-form" type="reset" value="Reset Filter">
		</form>
		`;

    this.registerEventListener("input", this, this.filterVisits);
    // this.registerEventListener("change", this, this.filterVisits);

    this.registerEventListener(
      "click",
      ".reset-form",

      (event) => {
        this.rendered.reset();
        // CustomEvents.sendEvent(CustomEvents.eventNames.reloadCards);
        this.filterVisits(event);
      }
    );
  }

  filterVisits = async (event) => {
    // event.preventDefault();
    const formData = this.formToObject();
    const visits = await getVisits();
    // console.log(visits);
    const filteredCardsId = filterController(visits, formData).map(
      (visitCard) => visitCard.id
    );
    // CustomEvents.sendEvent(CustomEvents.eventNames.reloadCards, {
    //   visitCards: result,
    // });

    const visitCardsSection = document.querySelector(".visit-cards-section");
    const visitCards = visitCardsSection.querySelectorAll(".visit-card");
    visitCards.forEach((visitCard) => {
      filteredCardsId.includes(Number(visitCard.dataset.id))
        ? visitCard.classList.remove("hidden")
        : visitCard.classList.add("hidden");
    });
    // console.log(result);
  };
}
