// export default class Visit {
//    constructor() {

//    }
// }
export class Visit {
   constructor(
     visitId = null,
     name,
     doctor,
     purpose,
     description,
     urgency,
     completed
   ) {
     this.id = visitId;
     this.name = name;
     this.doctor = doctor;
     this.purpose = purpose;
     this.description = description;
     this.urgency = urgency;
     this.completed = completed;
   }
 }
 